function createKey() {
    var Key = document.getElementById("key");
    var createButton = document.getElementById('create_key');
    var deleteButton = document.getElementById('delete_key');

    $.ajax({

        method: 'POST',
        url: '/api/v1/createKey',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            Key.innerText = data;
            createButton.setAttribute('style', 'display: none');
            deleteButton.setAttribute('style', 'display: block');
        }
    });
}

function deleteKey() {
    var conform = confirm("you are about deleting your key permanently ? ");
    if (!conform) return ;

    var Key = document.getElementById("key");
    var createButton = document.getElementById('create_key');
    var deleteButton = document.getElementById('delete_key');

    $.ajax({

        method: 'POST',
        url: '/api/v1/deleteKey',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (data) {
            console.log(data);
            if (parseInt(data) === 1) {
                key.innerText = "key was deleted";
                deleteButton.setAttribute('style', 'display: none');
            }
        }
    });
}
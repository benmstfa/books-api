<?php

namespace App\Http\Controllers;

use App\Author;
use App\Book;
use App\Category;
use App\Http\Resources\AuthorCollection;
use App\Http\Resources\BookCollection;
use App\Http\Resources\CategoryCollection;
use App\Http\Resources\PublisherCollection;
use App\Publisher;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function validateSearch(Request $request)
    {
        $request['q'] = filter_input($request['q'], FILTER_SANITIZE_STRING);

        switch ($request['type']) {
            case 'category':
                return CategoryCollection::collection(Category::search($request['q'])->get());
                break;

            case 'book':
                return BookCollection::collection(Book::search($request['q'])->get());
                break;

            case 'publisher':
                return PublisherCollection::collection(Publisher::search($request['q'])->get());
                break;

            case 'author':
                return AuthorCollection::collection(Author::search($request['q'])->get());
                break;

        }
    }
}

<?php

namespace App\Http\Resources;

use App\Book;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request['withBooks'] == "Y") {
            return [
                'name' => $this->name,
                'Info' => $this->description,
                'books' => BookCollection::collection(Book::where('category_id', $this->id)->get())
            ];
        } else {
            return [
                'name' => $this->name,
                'Info' => $this->description
            ];
        }

    }
}

<?php

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;



Auth::routes();


Route::prefix('api/v1')->group(function () {

    Route::get('/', 'HomeController@index')
        ->middleware('auth');

    Route::get('/home', 'HomeController@index')->name('home');



    // key routes
    Route::middleware('auth')->group(function () {
        Route::post('/createKey', [
            'uses' => 'KeyController@create',
            'as' => 'createKey',
        ]);

        Route::post('/deleteKey', [
            'uses' => 'KeyController@destroy',
            'as' => 'deleteKey',
        ]);
    });


});// end of prefix routes

Route::get('/try/{storyId}/{rate}', 'tryController@index');
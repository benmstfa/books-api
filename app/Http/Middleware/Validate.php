<?php

namespace App\Http\Middleware;

use Closure;
use App\Key;
use Illuminate\Support\Facades\Response;

class Validate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (isset($request['key'])) {
            $key = new Key;
            $key_found = $key->checkOnKey($request['key']);

            if ($key_found) {
                if ($key->increaseLimit($request['key'])) {
                    return $next($request);
                } else {
                    return $this->respond("sorry but you have exceeded the max allowed number of requests today");
                }

            } else {
                return $this->respond("this key not registered in our records");
            }
        } else {
            return $this->respond("where is your key");
        }
    }

    public function respond($message)
    {
        return Response::json([
            "message" => $message
        ]);
    }
}

<?php

namespace App\Http\Resources;

use App\Book;
use Illuminate\Http\Resources\Json\Resource;

class ReviewCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request['withBook'] == 'Y') {
            return [
                'book' => Book::where('id', $this->book_id)->first()->name,
                'reviewer' => $this->reviewer,
                'star' => $this->star,
                'description' => $this->description,
            ];
        } else {
            return [
                'book_id' => $this->book_id,
                'reviewer' => $this->reviewer,
                'star' => $this->star,
                'description' => $this->description,
            ];
        }
    }
}

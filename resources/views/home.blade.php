@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Your Key</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                                <h1></h1>
                            </div>
                        @endif
                        <div id="key" class="h1 key text-center" style="color: blue">
                            {{ isset($key->rkey) ? $key->rkey : "you don't have one "}}
                        </div>
                    </div>

                    <div class="card-footer">
                        <button id="create_key" class="float-right btn btn-primary" onclick="createKey()"
                                style="display: {{ isset($key) ? 'none' : 'block'}}">Create Key
                        </button>

                        <button id="delete_key" class="float-right btn btn-danger" onclick="deleteKey()"
                                style="display: {{ isset($key->rkey) ? 'block' : 'none' }}">Delete Key
                        </button>

                        <p>Keep Your key in safe place or just sign in.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<?php

namespace App\Http\Controllers;

use App\Key;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class KeyController extends Controller
{
    public function index()
    {

        return Auth::id();
    }



    public function create()
    {
        $new_key = new Key;
        return $new_key->createKey();
    }


    public function destroy()
    {
        $key = new Key;

        if ($key->deleteKey())
            return 1;

        return 0;
    }
}

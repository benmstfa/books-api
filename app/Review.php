<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Review extends Model
{
    use Eloquence;

    protected $table = 'reviews';

    protected $searchableColumns = ['name', 'description'];


    public function book()
    {
        return $this->belongsTo(Book::class);
    }
}

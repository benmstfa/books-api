<?php

use Faker\Generator as Faker;

$factory->define(App\Review::class, function (Faker $faker) {
    return [
        'book_id' => \App\Book::all()->random()->id,
        'reviewer' => $faker->name,
        'star' => rand(0, 5),
        'description' => $faker->paragraph(rand(1, 5))
    ];
});

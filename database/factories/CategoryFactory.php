<?php

use Faker\Generator as Faker;

$factory->define(App\Category::class, function (Faker $faker) {
    return [
        'description' => $faker->paragraph(rand(1, 5))
    ];
});

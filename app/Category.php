<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Category extends Model
{
    use Eloquence;

    protected $searchableColumns = ['name', 'description'];

    public function books() {
        return $this->hasMany(Book::class);
    }
}

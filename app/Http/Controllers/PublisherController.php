<?php

namespace App\Http\Controllers;

use App\Http\Resources\PublisherCollection;
use App\Http\Resources\PublisherResource;
use App\Publisher;
use Illuminate\Http\Request;

class PublisherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return PublisherCollection::collection(Publisher::all());
    }

    public function random() {
        return new PublisherResource(Publisher::all()->random());
    }
}

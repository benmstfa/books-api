<?php

use Faker\Generator as Faker;

$factory->define(App\Publisher::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'address' => $faker->address,
        'description' => $faker->paragraph(rand(1, 5)),
        'phone_number' => $faker->phoneNumber,
        'establish_date' => $faker->date('y-m-d'),
    ];
});

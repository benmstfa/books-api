<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Key extends Model
{
    protected $table = 'api_keys';

    protected $fillable = ['requests_number'];


    public function createKey()
    {


        $key = substr(base64_encode(md5(mt_rand())), 0, 32);
        $key = strtolower($key);
        $new_key = new Key;
        $new_key->user_id = Auth::id();
        $new_key->rkey = $key;
        $new_key->created_by = User::find(Auth::id())->name;

        if ($new_key->save()) {
            return $key;
        }

        return 0;
    }

    public function deleteKey()
    {
        $key = Key::where('user_id', Auth::id())->first();
        $key->rkey = null;
        if ($key->save()) {
            return true;
        }
        return false;
    }

    public function checkOnKey($request_key)
    {
        $key_found = $this->where('rkey', $request_key)->first();
        if ($key_found == null) {
            return false;
        }
        return true;
    }

    public function increaseLimit($request_key)
    {
        $user = $this->where('rkey', $request_key)->first();

        if ($this->checkOnKey($request_key)) {
            if ($user->requests_number < 200) {
                $user->update(['requests_number' => ($user->requests_number + 1)]);
                return true;
            }
            return false;
        }

    }

}

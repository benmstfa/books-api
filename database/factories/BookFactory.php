<?php

use Faker\Generator as Faker;

$factory->define(App\Book::class, function (Faker $faker) {
    return [
        'publisher_id' => \App\Publisher::all()->random()->id,
        'author_id' => \App\Author::all()->random()->id,
        'category_id' => \App\Category::all()->random()->id,
        'name' => $faker->name,
        'cover_path' => $faker->image('public/storage/images', 400, 300, null, false),
        'description' => $faker->paragraph(rand(1, 5)),
        'publish_date' => $faker->date('y-m-d', 'now')
    ];
});

<?php

namespace App\Http\Resources;

use App\Book;
use Illuminate\Http\Resources\Json\JsonResource;

class AuthorResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request['withBooks'] == "Y") {
            return [
                'name' => $this->name,
                'address' => $this->address,
                'Site Url' => $this->site_url,
                'Phone Number' => $this->phone_number,
                'Birth Day' => $this->birth_day,
                'books' => BookCollection::collection(Book::where('author_id', $this->id)->get())
            ];
        } else {
            return [
                'name' => $this->name,
                'address' => $this->address,
                'Site Url' => $this->site_url,
                'Phone Number' => $this->phone_number,
                'Birth Day' => $this->birth_day,
            ];
        }
    }

}

<?php

use Faker\Generator as Faker;

$factory->define(App\Author::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'address' => $faker->address,
        'site_url' => $faker->url,
        'phone_number' => $faker->phoneNumber,
        'birth_day' => $faker->date('y-m-d', 'now'),
        'created_by' => 'egydev',
        'description' => $faker->paragraph(rand(1, 5))
    ];
});

<?php

namespace App\Http\Resources;

use App\Book;
use Illuminate\Http\Resources\Json\JsonResource;

class PublisherResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request['withBooks'] == "Y") {
            return [
                'name' => $this->name,
                'address' => $this->address,
                'info' => $this->description,
                'phone_number' => $this->phone_number,
                'establish_date' => $this->establish_date,
                'books' => BookCollection::collection(Book::where('publisher_id', $this->id)->get())
            ];
        } else {
            return [
                'name' => $this->name,
                'address' => $this->address,
                'info' => $this->description,
                'phone_number' => $this->phone_number,
                'establish_date' => $this->establish_date,
            ];
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Author;
use App\Http\Resources\AuthorCollection;
use App\Http\Resources\AuthorResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        if (isset($request['key'])) return "lol";
        return AuthorCollection::collection(Author::all());
    }

    public function random() {
        return new AuthorResource(Author::all()->random());
    }

    public function search(Request $request) {
//        return AuthorCollection::collection(Author::search("man")->get());

        if (isset($request['q'])  && $request['q'] !== null){
            return AuthorCollection::collection(Author::search($request['q'])->get());
        }
    }
}

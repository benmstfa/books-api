<?php

namespace App\Http\Resources;

use App\Book;
use Illuminate\Http\Resources\Json\JsonResource;

class ReviewResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'book' => new BookResource(Book::where('id', $this->book_id)->first()),
            'reviewer' => $this->reviewer,
            'star' => $this->star,
            'description' => $this->description,
        ];
    }
}

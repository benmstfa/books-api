<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;



Route::prefix('/v1')->group(function () {

    Route::middleware('validate')->group(function () {
        // get collections
        Route::get('/books', 'BookController@index');

        Route::get('/authors', 'AuthorController@index');

        Route::get('/books', 'BookController@index');

        Route::get('/categories', 'CategoryController@index');

        Route::get('/publishers', 'PublisherController@index');

        Route::get('/reviews', 'ReviewController@index');

        // get random
        Route::get('/book/random', 'BookController@random');

        Route::get('/author/random', 'AuthorController@random');

        Route::get('/category/random', 'CategoryController@random');

        Route::get('/publisher/random', 'PublisherController@random');

        Route::get('/review/random', 'ReviewController@random');

        //search
        Route::get('/search', 'SearchController@validateSearch');

    });



});// end of prefix routes




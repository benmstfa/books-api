<?php

namespace App\Http\Resources;

use App\Author;
use App\Category;
use App\Publisher;
use App\Review;
use Illuminate\Http\Resources\Json\Resource;

class BookCollection extends Resource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        if ($request['withReviews'] == 'Y') {
            return [
                'name' => $this->name,
                'info' => $this->description,
                'publish_date' => $this->publish_date,
                'author' => Author::where('id', $this->author_id)->first()->name,
                'publisher' => Publisher::where('id', $this->publisher_id)->first()->name,
                'category' => Category::where('id', $this->category_id)->first()->name,
                'cover' => 'http://127.0.0.1:8000/storage/images/' . $this->cover_path,
                'reviews' => ReviewCollection::collection(Review::where('book_id', $this->id)->get())
            ];
        } else {
            return [
                'name' => $this->name,
                'info' => $this->description,
                'publish_date' => $this->publish_date,
                'author' => Author::where('id', $this->author_id)->first()->name,
                'publisher' => Publisher::where('id', $this->publisher_id)->first()->name,
                'category' => Category::where('id', $this->category_id)->first()->name,
                'cover' => 'http://127.0.0.1:8000/storage/images/' . $this->cover_path,
            ];
        }
    }
}

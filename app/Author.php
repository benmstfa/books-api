<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Author extends Model
{
    use Eloquence;

    protected $searchableColumns = ['name', 'description', 'phone_number', 'address'];

    public function books()
    {
        return $this->hasMany(Book::class);
    }

    public function searchableAs()
    {
        return 'name';
    }

}
